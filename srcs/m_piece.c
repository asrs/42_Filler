/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   m_piece.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/16 10:10:26 by clrichar          #+#    #+#             */
/*   Updated: 2018/07/17 19:23:05 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

static bool		piece_size(t_ply *data)
{
	int			rd;
	int			ret;
	char		*line;
	char		**tab;

	ret = 0;
	tab = NULL;
	line = NULL;
	if ((rd = get_next_line(0, &line)) < 0)
		ret = 1;
	if (ret == 0 && (!line || ft_strlen(line) < 9
				|| !ft_strnequ(line, "Piece ", 6)))
		ret = 1;
	if (ret == 0 && (!(tab = ft_strsplit(line, ' ')) || !tab[2]))
		ret = 1;
	if (ret == 0 && (!ft_overflow(tab[1]) || !ft_overflow(tab[2])))
		ret = 1;
	data->py = (ret == 0) ? ft_atoi(tab[1]) : 0;
	data->px = (ret == 0) ? ft_atoi(tab[2]) : 0;
	(line) ? ft_strdel(&line) : 0;
	(tab) ? ft_str_tabdel(&tab) : 0;
	return ((ret == 0) ? true : false);
}

static bool		piece_stock(t_ply *data, char **line, int n)
{
	int			i;
	int			ret;

	i = 0;
	ret = 0;
	if (!(*line) || ft_strlen(*line) != (size_t)data->px)
		ret = 1;
	while ((*line)[i])
	{
		if (ft_strnchr(".*", (*line)[i]) == 0)
		{
			ret = 1;
			break ;
		}
		i++;
	}
	(ret == 0) ? ft_strncpy(data->piece[n], *line, (size_t)(data->px)) : 0;
	(*line) ? ft_strdel(&(*line)) : 0;
	return ((ret == 0) ? true : false);
}

static bool		piece_get(t_ply *data, int rd, int n)
{
	char		*line;

	line = NULL;
	if (!data->piece)
	{
		if (!init_piece(data))
			return (false);
	}
	while ((rd = get_next_line(0, &line)) > 0)
	{
		if (!piece_stock(data, &line, n))
			return (false);
		n++;
		if (n == data->py)
			break ;
	}
	(line) ? ft_strdel(&line) : 0;
	return (true);
}

bool			m_piece(t_ply *data)
{
	if (!piece_size(data))
		return (false);
	if (!piece_get(data, 0, 0))
		return (false);
	return (true);
}
