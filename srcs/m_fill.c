/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   m_fill.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/17 18:02:06 by clrichar          #+#    #+#             */
/*   Updated: 2018/07/17 18:05:05 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

static bool			check_block(int y, int x, int *weld, t_ply *data)
{
	if (x < 0 || x >= data->mx)
		return (false);
	if (y < 0 || y >= data->my)
		return (false);
	if (data->map[y][x] == data->foe_char
			|| data->map[y][x] == data->foe_char + 32)
		return (false);
	if (data->map[y][x] == data->ply_char
			|| data->map[y][x] == data->ply_char + 32)
		*weld += 1;
	return (true);
}

static bool			test_bloc(int y, int x, int weld, t_ply *data)
{
	int				i;
	int				j;

	i = 0;
	weld = 0;
	while (i < data->py)
	{
		j = 0;
		while (j < data->px)
		{
			if (data->piece[i] && data->piece[i][j] == '*')
			{
				if (!check_block(y + i, x + j, &weld, data))
					return (false);
			}
			j++;
		}
		i++;
	}
	if (weld != 1)
		return (false);
	data->cy = y;
	data->cx = x;
	return (true);
}

static bool			set_bloc(int y, int x, t_ply *data)
{
	data->cy = y;
	data->cx = x;
	return (test_bloc(y, x, 0, data));
}

bool				m_fill(t_ply *data)
{
	int				i;
	int				j;

	i = 0;
	while (i < data->my)
	{
		j = 0;
		while (j < data->mx)
		{
			if (set_bloc(i, j, data))
				return (true);
			j++;
		}
		i++;
	}
	if (set_bloc(i, j, data))
		return (true);
	return (false);
}
