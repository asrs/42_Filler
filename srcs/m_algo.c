/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   m_algo.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/17 18:02:15 by clrichar          #+#    #+#             */
/*   Updated: 2018/07/17 18:02:17 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

static void		reset(t_ply *data)
{
	data->big = 0;
	data->cy = 0;
	data->cx = 0;
}

static bool		check_block(int y, int x, int *weld, t_ply *data)
{
	if (x < 0 || x >= data->mx)
		return (false);
	if (y < 0 || y >= data->my)
		return (false);
	if (data->map[y][x] == data->foe_char
			|| data->map[y][x] == data->foe_char + 32)
		return (false);
	if (data->map[y][x] == data->ply_char
			|| data->map[y][x] == data->ply_char + 32)
		*weld += 1;
	return (true);
}

static int		test_bloc(int y, int x, int score, t_ply *data)
{
	int			i;
	int			j;
	int			weld;

	i = 0;
	weld = 0;
	while (i < data->py)
	{
		j = 0;
		while (j < data->px)
		{
			if (data->piece[i] && data->piece[i][j] == '*')
			{
				if (!check_block(y + i, x + j, &weld, data))
					return (0);
				score += data->weight[y + i][x + j];
			}
			j++;
		}
		i++;
	}
	if (weld == 1)
		return ((score) ? score : 1);
	return (0);
}

void			m_algo(t_ply *data)
{
	int			i;
	int			j;
	int			score;

	i = 0 - (data->py);
	score = 0;
	reset(data);
	while (i < (data->my - 1))
	{
		j = 0 - (data->px);
		while (j < (data->mx - 1))
		{
			if ((score = test_bloc(i, j, 0, data)))
			{
				if (score > data->big)
				{
					data->big = score;
					data->cy = i;
					data->cx = j;
				}
			}
			j++;
		}
		i++;
	}
}
