/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   misc.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/17 11:12:23 by clrichar          #+#    #+#             */
/*   Updated: 2018/07/17 11:47:25 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

void			ft_set_area_diag(t_ply *data, int y, int x)
{
	int				sc;

	sc = (data->weight[y][x] == 0) ? 999 : data->weight[y][x];
	if ((y - 1) >= 0 && (x + 1) < data->mx && data->weight[y - 1][x + 1] > 0)
		sc = (data->weight[y - 1][x + 1] < sc) ? data->weight[y - 1][x + 1]
			: sc;
	if ((y - 1) >= 0 && (x - 1) >= 0 && data->weight[y - 1][x - 1] > 0)
		sc = (data->weight[y - 1][x - 1] < sc) ? data->weight[y - 1][x - 1]
			: sc;
	if ((y + 1) < data->my && (x + 1) < data->mx
			&& data->weight[y + 1][x + 1] > 0)
		sc = (data->weight[y + 1][x + 1] < sc) ? data->weight[y + 1][x + 1]
			: sc;
	if ((y + 1) < data->my && (x - 1) >= 0 && data->weight[y + 1][x - 1] > 0)
		sc = (data->weight[y + 1][x - 1] < sc) ? data->weight[y + 1][x - 1]
			: sc;
	if (sc < 999 && sc > 0 && data->weight[y][x] != 1)
		data->weight[y][x] = sc + 1;
}

void			ft_set_area_base(t_ply *data, int y, int x)
{
	int				sc;

	sc = (data->weight[y][x] == 0) ? 999 : data->weight[y][x];
	if (((y - 1) >= 0) && data->weight[y - 1][x] > 0)
		sc = (data->weight[y - 1][x] < sc) ? data->weight[y - 1][x] : sc;
	if (((y + 1) < data->my) && data->weight[y + 1][x] > 0)
		sc = (data->weight[y + 1][x] < sc) ? data->weight[y + 1][x] : sc;
	if (((x - 1) >= 0) && data->weight[y][x - 1] > 0)
		sc = (data->weight[y][x - 1] < sc) ? data->weight[y][x - 1] : sc;
	if (((x + 1) < data->mx) && data->weight[y][x + 1] > 0)
		sc = (data->weight[y][x + 1] < sc) ? data->weight[y][x + 1] : sc;
	if (sc < 999 && sc > 0 && data->weight[y][x] != 1)
		data->weight[y][x] = sc + 1;
}

void			ft_area_rounded(t_ply *data, int y, int x)
{
	int			n;

	n = 0;
	n = (((y - 1) >= 0) && data->weight[y - 1][x] < 0) ? (n + 1) : n;
	n = (((y + 1) < data->my) && data->weight[y + 1][x] < 0) ? (n + 1) : n;
	n = (((x - 1) >= 0) && data->weight[y][x - 1] < 0) ? (n + 1) : n;
	n = (((x + 1) < data->mx) && data->weight[y][x + 1] < 0) ? (n + 1) : n;
	if (n > 1 && data->weight[y][x] == 0)
		data->weight[y][x] = -1;
}

bool			ft_iszero(t_ply *data)
{
	int				i;
	int				j;
	int				n;

	i = 0;
	n = 0;
	while (i < data->my)
	{
		j = 0;
		while (j < data->mx)
		{
			if (data->weight[i][j] == 0)
				n++;
			j++;
		}
		i++;
	}
	return ((n == 0) ? true : false);
}
