/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_buffer.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/26 15:54:23 by clrichar          #+#    #+#             */
/*   Updated: 2018/06/29 17:39:37 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void			print(t_dna *dna, size_t len)
{
	dna->ret += (int)len;
	dna->stocked = 0;
	write(1, dna->buffer, len);
	ft_memset(dna->buffer, 0, BUFFER);
}

static void			stock(t_dna *dna, char **s, size_t space, size_t *len)
{
	if ((*len) < space)
	{
		ft_strncpy(&(dna->buffer[dna->stocked]), *s, *len);
		dna->stocked += *len;
		*len = 0;
		*s += *len;
		return ;
	}
	else if ((*len) >= space)
	{
		ft_strncpy(&(dna->buffer[dna->stocked]), *s, space);
		*len -= space;
		dna->stocked += space;
		*s += space;
	}
}

void				ft_buffer(char *s, size_t len)
{
	t_dna			*dna;
	size_t			space;

	dna = call();
	space = (size_t)(BUFFER - dna->stocked);
	while (len > 0)
	{
		stock(dna, &s, space, &len);
		if (dna->stocked == BUFFER)
			print(dna, BUFFER);
	}
}
